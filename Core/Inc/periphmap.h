#pragma once

#include "adc.h"
#include "cordic.h"
#include "dac.h"
#include "dma.h"
#include "fdcan.h"
#include "fmac.h"
#include "gpio.h"
#include "hrtim.h"
#include "i2c.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"

#include "gpio_util.h"

// ADC
extern ADC_HandleTypeDef *const phase_a_adc_handle;
extern ADC_HandleTypeDef *const phase_b_adc_handle;
extern ADC_HandleTypeDef *const phase_c_adc_handle;
extern ADC_HandleTypeDef *const vbus_singleend_adc_handle;

// CORDIC
extern CORDIC_HandleTypeDef *const cordic_handle;

// DAC
extern DAC_HandleTypeDef *const dac_aout1_handle;
extern const u32 dac_aout1_channel;
extern DAC_HandleTypeDef *const dac_aout2_handle;
extern const u32 dac_aout2_channel;

// DMA


extern DMA_HandleTypeDef *const phase_a_adc_dma;
extern DMA_HandleTypeDef *const phase_b_adc_dma;
extern DMA_HandleTypeDef *const phase_c_adc_dma;
extern DMA_HandleTypeDef *const vbus_singleend_adc_dma;

extern DMA_HandleTypeDef *const leds_spi_tx_dma;
extern DMA_HandleTypeDef *const encoder_spi_tx_dma;
extern DMA_HandleTypeDef *const encoder_spi_rx_dma;




// FDCAN
extern FDCAN_HandleTypeDef *const can_handle;

// FMAC
extern FMAC_HandleTypeDef *const fmac_handle;

// GPIO
extern const gpio_pin_t gpio1_pin;
extern const gpio_pin_t gpio2_pin;
extern const gpio_pin_t gpio3_pin;
extern const gpio_pin_t gpio4_pin;

extern const gpio_pin_t encoder_spi_nss;

// I2C
extern I2C_HandleTypeDef *const i2c_handle;

// SPI
extern SPI_HandleTypeDef *const leds_spi_handle;
extern SPI_HandleTypeDef *const encoder_spi_handle;

// TIM
extern TIM_HandleTypeDef *const status_led_tim_handle;
extern const u32 status_led_tim_channel;

// USART
extern UART_HandleTypeDef *const debug_uart_handle;

// HRTIM
extern HRTIM_HandleTypeDef *const bldc_hrtim_handle;
extern const u8 phase_a_hrtim_timer_handle;
extern const u8 phase_b_hrtim_timer_handle;
extern const u8 phase_c_hrtim_timer_handle;
extern const u8 master_hrtim_timer_handle;

extern const u32  phase_a_H_compare_unit;
extern const u32  phase_a_L_compare_unit;
extern const u32  phase_b_H_compare_unit;
extern const u32  phase_b_L_compare_unit;
extern const u32  phase_c_H_compare_unit;
extern const u32  phase_c_L_compare_unit;

extern const u32  phase_a_H_output;
extern const u32  phase_a_L_output;
extern const u32  phase_b_H_output;
extern const u32  phase_b_L_output;
extern const u32  phase_c_H_output;
extern const u32  phase_c_L_output;

// SYSTICK
extern TIM_TypeDef  *const timebase_source_timer;
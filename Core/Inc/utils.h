//
// Created by solomon on 2/17/22.
//

#pragma once

#include "typedefs.h"

static const f32 M_PI_f32 = 3.14159265358979323846f;
static const f32 one_over_sqrt3 = 0.57735026919f;
static const f32 two_over_sqrt3 = 1.15470053838f;
static const f32 sqrt3_over_2 = 0.86602540378f;

u32 micros();

void delay_us(u32 us);

//__attribute__((optimize("-fno-finite-math-only")))
inline bool is_nan(f32 x) {
    return __builtin_isnan(x);
}

#define MAX(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })
#define MIN(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

#define CLAMP(x, min, max) \
   ({ __typeof__ (x) _x = (x); \
    __typeof__ (min) _min = (min); \
       __typeof__ (max) _max = (max); \
     _x < _min ? _min : (_x > _max ? _max : x); })

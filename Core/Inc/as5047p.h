//
// Created by solomon on 2/21/22.
//

#pragma once

#include "typedefs.h"
#include "stm32g4xx_hal.h"

//extern volatile f32 current_angle_dma;
//extern volatile u8 current_angle_dma_raw[2];
//extern volatile u32 angle_timestamp_dma;
//extern volatile u32 angle_timestamp_completed_dma;
//extern volatile bool encoder_read_success_dma;
//extern volatile bool encoder_dma_in_progress;

extern volatile u16 current_angle_dma_raw;
extern volatile u32 angle_timestamp_dma;
extern volatile u32 angle_timestamp_completed_dma;
extern volatile bool encoder_read_success_dma;
extern volatile bool encoder_dma_in_progress;

typedef struct AS5047P_REG_ERROR {
    bool PARITY_ERROR;
    bool INVALID_COMMAND_ERROR;
    bool FRAMING_ERROR;
} AS5047P_REG_ERROR_t;

typedef struct AS5047P_REG_DIAG {
    bool FIELD_TOO_LOW;
    bool FIELD_TOO_HIGH;
    bool CORDIC_OVERFLOW;
    bool OFFSET_COMPENSATION_READY;
    u8 AGC_VALUE;
} AS5047P_REG_DIAG_t;

typedef struct AS5047P_REG_MAG {
    u16 CORDIC_MAGNITUDE;
} AS5047P_REG_MAG_t;

typedef struct AS5047P_REG_ANGLEUNC {
    u16 ANGLE_UNCOMPENSATED;
} AS5047P_REG_ANGLEUNC_t;

typedef struct AS5047P_REG_ANGLECOM {
    u16 ANGLE_COMPENSATED;
} AS5047P_REG_ANGLECOM_t;

typedef enum AS5047P_READ_STATUS {
    AS5047P_READ_OK=0,
    AS5047P_READ_HAL_ERROR=1,
    AS5047P_READ_HAL_BUSY=2,
    AS5047P_READ_HAL_TIMEOUT=3,
    AS5047P_READ_ERROR=4,
    AS5047P_READ_PARITY_ERROR=5,
    AS5047P_SOLOMON_FIX_UR_CODE=6,
} as5047p_read_status_t;

as5047p_read_status_t as5047p_read_error_reg(AS5047P_REG_ERROR_t *reg_error);
as5047p_read_status_t as5047p_read_diag_reg(AS5047P_REG_DIAG_t *reg_diag);
as5047p_read_status_t as5047p_read_cordic_magnitude_reg(AS5047P_REG_MAG_t *reg_magnitude);
as5047p_read_status_t as5047p_read_angle_uncompensated_reg(AS5047P_REG_ANGLEUNC_t *reg_angle_uncompensated);
as5047p_read_status_t as5047p_read_angle_compensated_reg(AS5047P_REG_ANGLECOM_t *reg_angle_compensated);

void as5047p_setup_dma_transaction();

bool as5047p_trigger_dma_read();

void as5047p_preheat();
u8 ams_parity(u16 v);
void encoder_spi_read_benchmark();

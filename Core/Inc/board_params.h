//
// Created by solomon on 2/17/22.
//

#pragma once

#define SHUNT_RESISTANCE (10e-3f)

#define HRTIM_PERIOD_CLOCKS 8192 // Number of clock ticks in HRTIM

// Longest time we can wait between control loop update and modulation update
#define MAX_CONTROL_LOOP_UPDATE_TO_CURRENT_UPDATE_DELTA (HRTIM_PERIOD_CLOCKS / 2 + 1*128)

#define HRTIM_CLK_BASE_HZ 160000000
#define HRTIM_CLK_MULT 32
#define HRTIM_CLK_EQUIV_HZ (HRTIM_CLK_BASE_HZ * HRTIM_CLK_MULT)

static const f32 HRTIM_CLK_BASE_HZ_f32 = (f32) HRTIM_CLK_BASE_HZ;
static const f32 HRTIM_CLK_MULT_f32 = (f32) HRTIM_CLK_MULT;
static const f32 HRTIM_CLK_EQUIV_HZ_f32 = HRTIM_CLK_BASE_HZ_f32 * HRTIM_CLK_MULT_f32;

// Current measurement period in seconds
static const f32 current_meas_period_f32;

// Current measurement frequency in hz
static const f32 current_meas_hz_f32;

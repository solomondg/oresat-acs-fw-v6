//
// Created by solomon on 2/17/22.
//

#pragma once

#include "typedefs.h"
#include "error.h"
#include "bldc_mathutil.h"

typedef struct pi_gains {
    f32 p; // V/A
    f32 i; // V/As
} pi_gains_t;

typedef struct foc_alphabeta {
    f32 alpha;
    f32 beta;
} foc_alphabeta_t;

typedef struct foc_abc {
    f32 A;
    f32 B;
    f32 C;
} foc_abc_t;

typedef struct foc_dq {
    f32 d;
    f32 q;
} foc_dq_t;

pi_gains_t foc_get_pi_gains();

void foc_set_pi_gains(pi_gains_t new_gains);

motor_error_t foc_register_measurement(f32 vbus_voltage, foc_abc_t I_phases, u32 input_timestamp);

//motor_error_t foc_register_measurement(f32 vbus_voltage, foc_alphabeta_t i_alphabeta, u32 input_timestamp);
motor_error_t foc_get_alpha_beta(u32 output_timestamp, foc_alphabeta_t *mod_alphabeta_out, f32 *ibus_out);

motor_error_t foc_get_output(u32 output_timestamp, svm_magnitudes_t *pwm_timings_out, f32 *ibus_out);

void foc_reset();

void foc_update_internal_setpoints(u32 timestamp);
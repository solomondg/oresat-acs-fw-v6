//
// Created by solomon on 2/16/22.
//

#pragma once

#include "typedefs.h"

#define SINCOS_FNCS_USE_CMSIS
//#define SINCOS_FNCS_USE_CORDIC
//#define SINCOS_FNCS_USE_CMATH


#if defined(SINCOS_FNCS_USE_CMSIS) || defined(SINCOS_FNCS_USE_CMATH)
#define SINCOS_FNCS_ARE_PURE
#endif


#define __cordic_fn_attr
#define __cmath_fn_attr __fn_const
#define __cmsis_fn_attr __fn_const

#if defined(SINCOS_FNCS_USE_CMSIS)
#define __sincos_attr __cmsis_fn_attr

#elif defined(SINCOS_FNCS_USE_CMATH)
#define __sincos_attr __cmath_fn_attr

#elif defined(SINCOS_FNCS_USE_CORDIC)
#define __sincos_attr __cordic_fn_attr
#endif

#if defined(SINCOS_FNCS_ARE_PURE)
#define __sincos_reliant_attr __sincos_attr
#endif


// ABC phase magnitude
typedef struct svm_magnitude {
    f32 A;
    f32 B;
    f32 C;
} svm_magnitudes_t;

/*
typedef struct three_phase_current {
    f32 A;
    f32 B;
    f32 C;
} three_phase_current_t;

typedef struct alphabeta_frame_current {
    f32 alpha;
    f32 beta;
} alphabeta_frame_current_t;

three_phase_current_t (alphabeta_frame_current_t alphabeta_currents);

alphabeta_frame_current_t clarke_transform(three_phase_current_t phase_currents);
 */

svm_magnitudes_t SVM(f32 alpha, f32 beta);

bool svm_output_valid(svm_magnitudes_t svm_magnitudes);


// Return type from simultanious sin/cos function
typedef struct sincos {
    f32 sin;
    f32 cos;
} sincos_t;

// Our chosen function implementations of sin/cos
f32 sin_f32(f32 x);

f32 cos_f32(f32 x);

f32 sqrt_f32(f32 x);

sincos_t sincos_f32(f32 x);

i32 round_f32(f32 x);

f32 fast_atan2(f32 y, f32 x);

f32 min_f32(f32 a, f32 b);

f32 max_f32(f32 a, f32 b);

f32 clamp_f32(f32 lower, f32 x, f32 upper);

f32 abs_f32(f32 x);

/*

sincos_t sincos_cordic(f32 x);

sincos_t sincos_cordic_raw(f32 x);

sincos_t sincos_cmsis(f32 x);

sincos_t sincos_cmath(f32 x);

f32 sin_cmsis(f32 x);

f32 cos_cmsis(f32 x);

f32 sin_cmath(f32 x);

f32 cos_cmath(f32 x);

 */
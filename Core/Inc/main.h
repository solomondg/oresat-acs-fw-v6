/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */


/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define ENC_A_Pin GPIO_PIN_0
#define ENC_A_GPIO_Port GPIOC
#define ENC_B_Pin GPIO_PIN_1
#define ENC_B_GPIO_Port GPIOC
#define PHA_SNS__Pin GPIO_PIN_0
#define PHA_SNS__GPIO_Port GPIOA
#define PHA_SNS_A1_Pin GPIO_PIN_1
#define PHA_SNS_A1_GPIO_Port GPIOA
#define PHB_SNS__Pin GPIO_PIN_2
#define PHB_SNS__GPIO_Port GPIOA
#define PHB_SNS_A3_Pin GPIO_PIN_3
#define PHB_SNS_A3_GPIO_Port GPIOA
#define AOUT2_Pin GPIO_PIN_5
#define AOUT2_GPIO_Port GPIOA
#define AOUT3_Pin GPIO_PIN_6
#define AOUT3_GPIO_Port GPIOA
#define GPIO1_Pin GPIO_PIN_7
#define GPIO1_GPIO_Port GPIOA
#define GPIO2_Pin GPIO_PIN_4
#define GPIO2_GPIO_Port GPIOC
#define GPIO3_Pin GPIO_PIN_5
#define GPIO3_GPIO_Port GPIOC
#define GPIO4_Pin GPIO_PIN_0
#define GPIO4_GPIO_Port GPIOB
#define VBUS_SNS_Pin GPIO_PIN_1
#define VBUS_SNS_GPIO_Port GPIOB
#define PHC_SNS__Pin GPIO_PIN_12
#define PHC_SNS__GPIO_Port GPIOB
#define PHC_SNS_B14_Pin GPIO_PIN_14
#define PHC_SNS_B14_GPIO_Port GPIOB
#define SPI_SOFTNSS_Pin GPIO_PIN_2
#define SPI_SOFTNSS_GPIO_Port GPIOD
#define STATUS_LED_Pin GPIO_PIN_6
#define STATUS_LED_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

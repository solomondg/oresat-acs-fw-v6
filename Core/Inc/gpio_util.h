//
// Created by solomon on 2/16/22.
//


#pragma once

#include "typedefs.h"
#include "gpio.h"

typedef struct gpio_pin {
    GPIO_TypeDef *port;
    u16 pin;
} gpio_pin_t;

bool gpio_pin_read(gpio_pin_t pin);
void gpio_pin_write(gpio_pin_t pin, bool state);

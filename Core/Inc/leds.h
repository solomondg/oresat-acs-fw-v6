//
// Created by solomon on 1/2/22.
//

#pragma once

#include "typedefs.h"

typedef struct rgb {
    u8 r, g, b;
} rgb_t;

typedef struct hsv {
    u8 h, s, v;
} hsv_t;

typedef enum hsv_conv_method {
    HSV_RAW, HSV_RAINBOW, HSV_SPECTRUM
} hsv_conv_method_t;

typedef enum leds {
    LED_STATUS, LED_SPEED, LED_CURRENT
} leds_t;

rgb_t __fn_const hsv_to_rgb(hsv_t hsv_in, hsv_conv_method_t conv_method);

void set_led(rgb_t color, leds_t LED);

void led_setup();

bool write_leds();

void set_status_led(u8 brightness);

void set_led_global_brightness(u8 bright);

//
// Created by solomon on 1/2/22.
//

#include <math.h>
#include "leds.h"
#include "dma.h"
#include "periphmap.h"
#include "printf.h"

#define APA102_BUFFER_SIZE (4 + 4*3 + 4)
u8 APA102_BUFFER[APA102_BUFFER_SIZE] = {
        0, 0, 0, 0, // Start frame
        0xFF, 0, 0, 0, // First LED (status)
        0xFF, 0, 0, 0, // Second LED (speed)
        0xFF, 0, 0, 0, // Third LED (current)
        0xFF, 0xFF, 0xFF, 0xFF // End frame
};


volatile bool is_spi_xfer_complete = true;

void spi_xfer_complete_callback(SPI_HandleTypeDef *hspi) {
    is_spi_xfer_complete = true;
}


void set_led_global_brightness(const u8 bright) {
    u8 bright_lower5 = (0b111 << 5) | (bright & 0b11111);
    APA102_BUFFER[4] = bright_lower5;
    APA102_BUFFER[8] = bright_lower5;
    APA102_BUFFER[12] = bright_lower5;
}

void led_setup() {
    HAL_TIM_PWM_Start(status_led_tim_handle, status_led_tim_channel);
    set_status_led(0);
    HAL_SPI_RegisterCallback(leds_spi_handle, HAL_SPI_TX_COMPLETE_CB_ID, &spi_xfer_complete_callback);
}

static bool transmit_sync() { // TODO only copy what's needed
    //printf("Transmit...\r\n");
    HAL_StatusTypeDef status = HAL_SPI_Transmit(leds_spi_handle, APA102_BUFFER,
                                                APA102_BUFFER_SIZE, 0xFFFF);
    return (status == HAL_OK);
}

static bool transmit_it() { // TODO only copy what's needed
    //printf("Transmit...\r\n");
    HAL_StatusTypeDef status = HAL_SPI_Transmit_IT(leds_spi_handle, APA102_BUFFER,
                                                   APA102_BUFFER_SIZE);
    return (status == HAL_OK);
}

static bool transmit_dma() { // TODO only copy what's needed
    if (!is_spi_xfer_complete) return false;
    is_spi_xfer_complete = false;
    HAL_StatusTypeDef status = HAL_SPI_Transmit_DMA(leds_spi_handle, APA102_BUFFER,
                                                    APA102_BUFFER_SIZE);
    return (status == HAL_OK);
}

static bool transmit_dma_poll() { // TODO only copy what's needed
    while (!is_spi_xfer_complete);
    is_spi_xfer_complete = false;
    HAL_StatusTypeDef status = HAL_SPI_Transmit_DMA(leds_spi_handle, APA102_BUFFER,
                                                    APA102_BUFFER_SIZE);
    return (status == HAL_OK);
}

__fn_flatten bool write_leds() {
    return transmit_dma_poll();
}

__fn_inline void set_status_led(const u8 brightness) {
    __HAL_TIM_SET_COMPARE(status_led_tim_handle, status_led_tim_channel, brightness);
}

void set_led(rgb_t color, leds_t LED) {
    u8 start_idx = 5;
    if (LED == LED_SPEED) start_idx = 9;
    else if (LED == LED_CURRENT) start_idx = 13;

    APA102_BUFFER[start_idx++] = color.b;
    APA102_BUFFER[start_idx++] = color.g;
    APA102_BUFFER[start_idx] = color.r;
}

static __fn_const rgb_t hsv2rgb_spectrum(hsv_t hsv);

static __fn_const rgb_t hsv2rgb_rainbow(hsv_t hsv);

static __fn_const rgb_t hsv2rgb_raw(hsv_t hsv);


__fn_const __fn_flatten rgb_t hsv_to_rgb(hsv_t hsv_in, hsv_conv_method_t conv_method) {
    switch (conv_method) {
        default:
        case HSV_RAW:
            return hsv2rgb_raw(hsv_in);
        case HSV_RAINBOW:
            return hsv2rgb_rainbow(hsv_in);
        case HSV_SPECTRUM:
            return hsv2rgb_spectrum(hsv_in);
    }
}

/* *** *** *** *** HERE YE BE DRAGONS *** *** *** *** */

typedef u8 fract8;

static __fn_const __fn_inline u8 scale8(const u8 i, const fract8 scale) {
    return (((u16) i) * (1 + ((u16) scale))) >> 8;
}

static __fn_const __fn_inline u8 scale8_video(const u8 i, const fract8 scale) {
    return ((((u16) i) * ((u16) scale)) >> 8) + ((i && scale) ? 1 : 0);
}

#define APPLY_DIMMING(X) (X)
#define HSV_SECTION_6 (0x20) // 32
#define HSV_SECTION_3 (0x40)  // 64
#define K255 255
#define K171 171
#define K170 170
#define K85  85

static __fn_const __fn_inline rgb_t hsv2rgb_spectrum(const hsv_t hsv) {
    return hsv2rgb_raw((hsv_t) {scale8(hsv.h, 191), hsv.s, hsv.v});
}

static __fn_const rgb_t hsv2rgb_rainbow(const hsv_t hsv) {
    const u8 Y1 = 1; // Moderate yellow boost
    const u8 Y2 = 0; // Strong yellow boost
    const u8 G2 = 0; // Divide all greens by 2
    const u8 Gscale = 0; // What to scale greens down by
    u8 hue = hsv.h;
    u8 sat = hsv.s;
    u8 val = hsv.v;
    u8 offset = hue & 0x1F; // 0..31
    u8 offset8 = offset << 3; // offset8 = offset*8
    u8 third = scale8(offset8, 85); // max = 85
    u8 r, g, b;
    if (!(hue & 0x80)) {
        if (!(hue & 0x40)) {
            if (!(hue & 0x20)) {
                r = K255 - third;
                g = third;
                b = 0;
            } else {
                if (Y1) {
                    r = K171;
                    g = K85 + third;
                    b = 0;
                }
                if (Y2) {
                    r = K170 + third;
                    u8 twothirds = scale8(offset8, ((256 * 2) / 3)); // max=170
                    g = K85 + twothirds;
                    b = 0;
                }
            }
        } else {
            if (!(hue & 0x20)) {
                if (Y1) {
                    //u8 twothirds = scale8(offset8, ((256 * 2) / 3));
                    u8 twothirds = scale8(offset8, 170);
                    r = K171 - twothirds;
                    g = K170 + third;
                    b = 0;
                }
                if (Y2) {
                    r = K255 - offset8;
                    g = K255;
                    b = 0;
                }
            } else {
                r = 0;
                g = K255 - third;
                b = third;
            }
        }
    } else {
        if (!(hue & 0x40)) {
            if (!(hue & 0x20)) {
                r = 0;
                //u8 twothirds = scale8(offset8, ((256 * 2) / 3));
                u8 twothirds = scale8(offset8, 170);
                g = K171 - twothirds;
                b = K85 + twothirds;
            } else {
                r = third;
                g = 0;
                b = K255 - third;
            }
        } else {
            if (!(hue & 0x20)) {
                r = K85 + third;
                g = 0;
                b = K171 - third;
            } else {
                r = K170 + third;
                g = 0;
                b = K85 - third;
            }
        }
    }
    if (G2) g = g >> 1;
    if (Gscale) g = scale8_video(g, Gscale);
    if (sat != 255) {
        if (sat == 0) {
            r = 255;
            b = 255;
            g = 255;
        } else {
            u8 desat = 255 - sat;
            desat = scale8_video(desat, desat);

            u8 satscale = 255 - desat;

            r = scale8(r, satscale);
            g = scale8(g, satscale);
            b = scale8(b, satscale);

            u8 brightness_floor = desat;
            r += brightness_floor;
            g += brightness_floor;
            b += brightness_floor;
        }
    }
    if (val != 255) {
        val = scale8_video(val, val);
        if (val == 0) {
            r = 0;
            g = 0;
            b = 0;
        } else {
            r = scale8(r, val);
            g = scale8(g, val);
            b = scale8(b, val);
        }
    }
    return (rgb_t) {r, g, b};
}

static __fn_const rgb_t hsv2rgb_raw(const hsv_t hsv) {
    rgb_t rgb_local;

    const u8 value = APPLY_DIMMING(hsv.v);
    const u8 saturation = hsv.s;
    const u8 hue = hsv.h;
    const u8 invsat = APPLY_DIMMING(255 - saturation);
    const u8 brightness_floor = (value * invsat) / 256; // (value*invsat)/256
    const u8 color_amplitude = value - brightness_floor;
    const u8 section = hue / HSV_SECTION_3; // hue / HSV_SECTION_3
    const u8 offset = hue % HSV_SECTION_3;
    const u8 rampup = offset; // 0..63
    const u8 rampdown = (HSV_SECTION_3 - 1) - offset; // 63..0
    const u8 rampup_amp_adj = (((u16) rampup) * ((u16) color_amplitude)) >> 6; // (rampup*color_amplitude)/(256/4)
    const u8 rampdown_amp_adj = (((u16) rampdown) * ((u16) color_amplitude)) >> 6; // (rampdown*color_amplitude)/(256/4)
    const u8 rampup_adj_with_floor = rampup_amp_adj + brightness_floor;
    const u8 rampdown_adj_with_floor = rampdown_amp_adj + brightness_floor;
    if (section) {
        if (section == 1) { // Section 1
            rgb_local.r = brightness_floor;
            rgb_local.g = rampdown_adj_with_floor;
            rgb_local.b = rampup_adj_with_floor;
        } else { // Section 2
            rgb_local.r = rampup_adj_with_floor;
            rgb_local.g = brightness_floor;
            rgb_local.b = rampdown_adj_with_floor;
        }
    } else { // Section 0
        rgb_local.r = rampdown_adj_with_floor;
        rgb_local.g = rampup_adj_with_floor;
        rgb_local.b = brightness_floor;
    }
    return rgb_local;
}
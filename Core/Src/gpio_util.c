//
// Created by solomon on 2/16/22.
//

#include "gpio_util.h"

__fn_flatten bool gpio_pin_read(const gpio_pin_t pin) {
    GPIO_PinState bitstatus;

    /* Check the parameters */
    assert_param(IS_GPIO_PIN(pin.pin));

    if ((pin.port->IDR & pin.pin) != 0x00U)
    {
        bitstatus = GPIO_PIN_SET;
    }
    else
    {
        bitstatus = GPIO_PIN_RESET;
    }

    return bitstatus != GPIO_PIN_RESET;
}

__fn_flatten void gpio_pin_write(const gpio_pin_t pin, const bool state) {
    // Copied from HAL


    GPIO_PinState pinstate = state ? GPIO_PIN_SET : GPIO_PIN_RESET;
    //HAL_GPIO_WritePin(pin.port, pin.pin, state ? GPIO_PIN_SET : GPIO_PIN_RESET);
    //GPIO_PinState PinState = state ? GPIO_PIN_SET : GPIO_PIN_RESET;

    assert_param(IS_GPIO_PIN(pin.pin));
    assert_param(IS_GPIO_PIN_ACTION(pinstate));

    if (pinstate!= GPIO_PIN_RESET)
    {
        pin.port->BSRR = (uint32_t)pin.pin;
    }
    else
    {
        pin.port->BRR = (uint32_t)pin.pin;
    }
}

//
// Created by solomon on 2/17/22.
//

#include <math.h>
#include <stdlib.h>
#include "foc.h"
#include "cmsis_compiler.h"
#include "utils.h"

#include "board_params.h"
#include "bldc_mathutil.h"

//#define one_over_root3 0.57735026919f
//#define two_over_root3 1.15470053838f

// --------- INTERNAL VARIABLES ---------

// Config
pi_gains_t pi_gains = {NAN, NAN};

// External inputs - updated nondeterministically
bool enable_current_control_input = false;
foc_dq_t I_dq_setpoint_input; // [A] d/q setpoint
foc_dq_t V_dq_setpoint_input; // [V] d/q feed-forward voltage term
f32 motor_phase_position_rad_input; // [rad] motor electrical position
f32 motor_phase_vel_rads_input; // [rad/s] motor electrical velocity

// Internal setpoints - only set from within this compilation unit, on foc_update_internal_setpoints()
bool enable_current_control = false;
u32 ctrl_timestamp; // [HRTIM ticks] timestamp when we ran controller
foc_dq_t I_dq_setpoint = {NAN, NAN}; // [A] d/q setpoint
foc_dq_t V_dq_setpoint = {NAN, NAN}; // [V] d/q feed-forward voltage term
f32 motor_phase_position_rad = NAN; // [rad] motor electrical position
f32 motor_phase_vel_rads = NAN; // [rad/s] motor electrical velocity

// Internal variables - updated inside register_measurement() and get_alpha_beta()
u32 I_meas_timestamp; // [HRTIM ticks] Timestamp when we measured phase currents
f32 vbus_voltage_measurement = NAN; // [V] measured vbus voltage
foc_alphabeta_t I_alphabeta_measured = {NAN, NAN}; // [A, A] measured alpha/beta currents
foc_dq_t I_dq_measured; // [A, A] measured d/q axis currents
foc_dq_t PI_integrator = {0.f, 0.f}; // [V, V] PI intergal term
foc_alphabeta_t final_voltage_alphabeta = {0.f,
                                           0.f}; // [V, V] final computed alpha/beta voltages - will be SVM modulated into phase voltages
f32 power = 0.f; // [W] dot product of Vdq and Idq


pi_gains_t foc_get_pi_gains() {
    return pi_gains;
}

void foc_set_pi_gains(const pi_gains_t new_gains) {
    pi_gains.p = new_gains.p;
    pi_gains.i = new_gains.i;
}

// Clarke transform - convert from A/B/C phase currents to alpha/beta frame currents
static inline __fn_const foc_alphabeta_t clarke_transform(const foc_abc_t I_phases) {
    foc_alphabeta_t I_alphabeta;
    // Alpha = 2/3 * (Ia + -1/2 Ib + -1/2 Ic
    I_alphabeta.alpha = 2.f / 3.f * (1.f * I_phases.A + -1.f / 2.f * I_phases.B + -1.f / 2.f * I_phases.C);
    // Beta = 2/3 * (0 Ia + sqrt3/2 Ib + -sqrt3/2 Ic
    I_alphabeta.beta = 2.f / 3.f * (0.f * I_phases.A + sqrt3_over_2 * I_phases.B + -sqrt3_over_2 * I_phases.C);

    return I_alphabeta;
}

// Simplified Clarke transform from ODrive source (verified in Mathematica) - convert from A/B/C phase currents to alpha/beta frame currents
static inline __fn_const foc_alphabeta_t clarke_transform_simplified(const foc_abc_t I_phases) {
    foc_alphabeta_t I_alphabeta;
    I_alphabeta.alpha = I_phases.A; // Ia = -Ib-Ic
    I_alphabeta.beta = one_over_sqrt3 * (I_phases.B - I_phases.C);
    return I_alphabeta;
}

// Registers phase current + vbus voltage measurement. Called once per switching cycle.
motor_error_t foc_register_measurement(const f32 vbus_voltage, const foc_abc_t I_phases, const u32 input_timestamp) {

    // Simplified Clarke transform - convert from A/B/C phase currents to alpha/beta frame currents
    foc_alphabeta_t I_alphabeta;
    //I_alphabeta.alpha = I_phases.A;
    //I_alphabeta.beta = one_over_sqrt3 * (I_phases.B - I_phases.C);
    I_alphabeta = clarke_transform_simplified(I_phases);

    // Copy external args + computed alpha/beta from Clarke transform to internal variables
    I_meas_timestamp = input_timestamp;
    vbus_voltage_measurement = vbus_voltage;
    I_alphabeta_measured.alpha = I_alphabeta.alpha;
    I_alphabeta_measured.beta = I_alphabeta.beta;


    return MOTOR_ERROR_NONE;
}

static inline __sincos_reliant_attr foc_dq_t park_transform_simplified(const foc_alphabeta_t I_alpha_beta, const f32 motor_phase) {
    foc_dq_t I_dq_ret;

    f32 I_alpha = I_alpha_beta.alpha;
    f32 I_beta = I_alpha_beta.beta;

    f32 I_phase = motor_phase;

    sincos_t I_phase_sincos = sincos_f32(I_phase);
    f32 I_phase_sin = I_phase_sincos.sin;
    f32 I_phase_cos = I_phase_sincos.cos;

    I_dq_ret.d = I_phase_cos * I_alpha + I_phase_sin * I_beta;
    I_dq_ret.q = I_phase_cos * I_beta - I_phase_sin * I_alpha;

    return I_dq_ret;
}



motor_error_t foc_get_alpha_beta(const u32 output_timestamp, foc_alphabeta_t *mod_alphabeta_out, f32 *ibus_out) {
    // Check current/voltage measurement as well as measurement timestamps
    if (is_nan(vbus_voltage_measurement) || is_nan(I_alphabeta_measured.alpha) || is_nan(I_alphabeta_measured.beta))
        return MOTOR_ERROR_CONTROLLER_INITIALIZING; // Haven't recieved a current measurement yet
    else if (abs(((i32) I_meas_timestamp - (i32) ctrl_timestamp)) > MAX_CONTROL_LOOP_UPDATE_TO_CURRENT_UPDATE_DELTA)
        return MOTOR_ERROR_BAD_TIMING; // Too long delay between control loop and current measurement

    if (is_nan(V_dq_setpoint.d) || is_nan(V_dq_setpoint.q))
        return MOTOR_ERROR_VOLTAGE_SETPOINT_IS_NAN; // V_dq_setpoint contains NaN (hasn't been set)
    else if (is_nan(motor_phase_position_rad) || is_nan(motor_phase_vel_rads))
        return MOTOR_ERROR_PHASE_IS_NAN; // Motor phase sense contains NaN (hasn't been set)
    else if (is_nan(vbus_voltage_measurement))
        return MOTOR_ERROR_VBUS_IS_NAN; // Vbus voltage measurement is NaN (hasn't bene set)

    f32 Vd = V_dq_setpoint.d;
    f32 Vq = V_dq_setpoint.q;
    f32 motor_phase = motor_phase_position_rad;
    f32 motor_phase_vel = motor_phase_vel_rads;
    f32 vbus_voltage = vbus_voltage_measurement;

    foc_dq_t I_dq = {NAN, NAN};

    if (is_nan(I_alphabeta_measured.alpha) || is_nan(I_alphabeta_measured.beta)) {
        I_dq_measured.d = 0.f; // Fallback to zero computed d/q if alpha/beta is NaN
        I_dq_measured.q = 0.f;
    } else {
        // Park transform
        f32 motor_phase_extrap = // Extrapolated motor phase
                motor_phase_vel * (((f32) ((i32) I_meas_timestamp - (i32) ctrl_timestamp)) / HRTIM_CLK_BASE_HZ_f32);
        f32 I_phase = motor_phase + motor_phase_extrap; // Current motor phase for park transform

        I_dq = park_transform_simplified(I_alphabeta_measured, I_phase);

        I_dq_measured.d = I_dq.d;
        I_dq_measured.q = I_dq.q;
    }

    f32 mod_to_V = (2.f/3.f) * vbus_voltage;
    f32 V_to_mod = 1.f/mod_to_V;

    foc_dq_t mod_dq;

    if (!enable_current_control) { // Voltage control modulation
        //mod_dq = (foc_dq_t) {V_to_mod*Vd, V_to_mod*Vq};
        mod_dq.d = V_to_mod * Vd;
        mod_dq.q = V_to_mod * Vq;
    } else { // Current control modulation

        if (is_nan(pi_gains.p) || is_nan(pi_gains.i))
            return MOTOR_ERROR_PI_GAINS_IS_NAN;
        else if (is_nan(I_dq.d) || is_nan(I_dq.q))
            return MOTOR_ERROR_CURRENT_MEASUREMENT_IS_NAN;
        else if (is_nan(I_dq_setpoint.d) || is_nan (I_dq_setpoint.q))
            return MOTOR_ERROR_CURRENT_SETPOINT_IS_NAN;

        foc_dq_t I_err;
        I_err.d = I_dq_setpoint.d - I_dq.d;
        I_err.q = I_dq_setpoint.q - I_dq.q;

        // Apply PI term with V(d,q) setpoint acting as a feed-forward term
        mod_dq.d = V_to_mod * (Vd + PI_integrator.d + I_err.d * pi_gains.p);
        mod_dq.q = V_to_mod * (Vq + PI_integrator.q + I_err.q * pi_gains.p);

        // Saturate vector mod and lock integrator
        f32 mod_scalefactor = 0.8f * sqrt3_over_2 * 1.f / (sqrt_f32(mod_dq.d * mod_dq.d + mod_dq.q * mod_dq.q)); // TODO fast invsqrt?
        // If we're saturated
        if (mod_scalefactor < 1.f) {
            mod_dq.d *= mod_scalefactor;
            mod_dq.q *= mod_scalefactor;

            // Integral decay factor
            PI_integrator.d *= 0.99f;
            PI_integrator.q *= 0.99f;
        }
        else {
            PI_integrator.d += I_err.d * (pi_gains.i * current_meas_period_f32);
            PI_integrator.q += I_err.q * (pi_gains.i * current_meas_period_f32);
        }
    }

    // Inverse Park transform
    f32 motor_phase_extrap = // Extrapolated motor phase
            motor_phase_vel * (((f32) ((i32) output_timestamp - (i32) ctrl_timestamp)) / HRTIM_CLK_BASE_HZ_f32);
    f32 pwm_phase = motor_phase + motor_phase_extrap;

    sincos_t pwm_phase_sincos = sincos_f32(pwm_phase);
    f32 pwm_phase_sin = pwm_phase_sincos.sin;
    f32 pwm_phase_cos = pwm_phase_sincos.cos;

    foc_alphabeta_t mod_alphabeta;
    mod_alphabeta.alpha = pwm_phase_cos * mod_dq.d - pwm_phase_sin * mod_dq.q;
    mod_alphabeta.beta = pwm_phase_cos * mod_dq.q + pwm_phase_sin * mod_dq.d;
    // End inverse Park transform. TODO move to function

    final_voltage_alphabeta.alpha = mod_to_V * mod_alphabeta.alpha;
    final_voltage_alphabeta.beta = mod_to_V * mod_alphabeta.beta;

    mod_alphabeta_out->alpha = mod_alphabeta.alpha;
    mod_alphabeta_out->beta = mod_alphabeta.beta;

    if (!(is_nan(I_dq.d) || is_nan(I_dq.q))) {
        *ibus_out = mod_dq.d * I_dq.d + mod_dq.q * I_dq.q;
        power = vbus_voltage * (*ibus_out);
    }

    return MOTOR_ERROR_NONE;
}

// Computes output PWM modulation timings from current FOC internal state (PI controller + rotor angle)
motor_error_t foc_get_output(const u32 output_timestamp, svm_magnitudes_t *pwm_timings_out, f32 *ibus_out) {
    foc_alphabeta_t modulation_alpha_beta;
    motor_error_t status = foc_get_alpha_beta(output_timestamp, &modulation_alpha_beta, ibus_out);

    if (status != MOTOR_ERROR_NONE)  // Return foc_get_alpha_beta error
        return status;
    else if (is_nan(modulation_alpha_beta.alpha) || is_nan(modulation_alpha_beta.beta)) // Report if NaN outputs
        return MOTOR_ERROR_MODULATION_IS_NAN;

    // Go from alpha/beta modulation indices to SVM
    svm_magnitudes_t mag_abc = SVM(modulation_alpha_beta.alpha, modulation_alpha_beta.beta);
    bool svm_ok = svm_output_valid(mag_abc); // Check if SVM output is ok, return error if not
    if (!svm_ok)
        return MOTOR_ERROR_MODULATION_MAGNITUDE;

    pwm_timings_out->A = mag_abc.A;
    pwm_timings_out->B = mag_abc.B;
    pwm_timings_out->C = mag_abc.C;

    return MOTOR_ERROR_NONE;
}

void foc_reset() {
    PI_integrator.d = 0.f;
    PI_integrator.q = 0.f;
    vbus_voltage_measurement = NAN;
    I_alphabeta_measured.alpha = NAN;
    I_alphabeta_measured.beta = NAN;
    power = 0.f;
}

void foc_update_internal_setpoints(const u32 timestamp) {
    __disable_irq();

    ctrl_timestamp = timestamp;
    enable_current_control = enable_current_control_input;
    I_dq_setpoint.d = I_dq_setpoint_input.d;
    I_dq_setpoint.q = I_dq_setpoint_input.q;
    V_dq_setpoint.d = V_dq_setpoint.d;
    V_dq_setpoint.q = V_dq_setpoint.q;
    motor_phase_position_rad = motor_phase_position_rad_input;
    motor_phase_vel_rads = motor_phase_vel_rads_input;

    __enable_irq();
}
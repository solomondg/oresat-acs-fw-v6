/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "cordic.h"
#include "dac.h"
#include "dma.h"
#include "fdcan.h"
#include "fmac.h"
#include "hrtim.h"
#include "i2c.h"
#include "usart.h"
#include "spi.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include "typedefs.h"
#include "periphmap.h"

#include "leds.h"
#include "pwm.h"


#include "printf.h"
#include "bldc_mathutil.h"
#include "as5047p.h"
#include <math.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void ADC1_ISR(ADC_HandleTypeDef *hadc) {
    printf("ADC1 ISR\r\n");
}

void ADC2_ISR(ADC_HandleTypeDef *hadc) {
    printf("ADC2 ISR\r\n");
}

void ADC3_ISR(ADC_HandleTypeDef *hadc) {
    printf("ADC3 ISR\r\n");
}

void ADC4_ISR(ADC_HandleTypeDef *hadc) {
    printf("ADC4 ISR\r\n");
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void) {
    /* USER CODE BEGIN 1 */

    /* USER CODE END 1 */

    /* MCU Configuration--------------------------------------------------------*/

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* USER CODE BEGIN Init */

    /* USER CODE END Init */

    /* Configure the system clock */
    SystemClock_Config();

    /* USER CODE BEGIN SysInit */

    /* USER CODE END SysInit */

    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_DMA_Init();
    MX_CORDIC_Init();
    MX_DAC1_Init();
    MX_DAC2_Init();
    MX_FDCAN1_Init();
    MX_FMAC_Init();
    MX_HRTIM1_Init();
    MX_I2C1_Init();
    MX_LPUART1_UART_Init();
    MX_USART3_UART_Init();
    MX_SPI1_Init();
    MX_SPI3_Init();
    MX_TIM1_Init();
    MX_TIM4_Init();
    MX_ADC1_Init();
    MX_ADC2_Init();
    MX_ADC3_Init();
    MX_ADC4_Init();
    /* USER CODE BEGIN 2 */

    printf("\u033c\033[2J\033[300A");
    printf("Starting user code...\r\n");
    HAL_Delay(1);

    gpio_pin_write(encoder_spi_nss, true);
    HAL_Delay(20);


    led_setup();
    HAL_Delay(1);
    set_led_global_brightness(1);
    set_led((rgb_t) {0, 0, 0}, LED_STATUS);
    set_led((rgb_t) {0, 0, 0}, LED_CURRENT);
    set_led((rgb_t) {0, 0, 0}, LED_SPEED);
    write_leds();

    //u8 h = 0;

    printf("\r\n\r\n\r\n");


    __enable_irq();

    //hrtim_pwm_start();

    // hrtim_pwm_set_dcycle_u16(8192/3, 2 * 8192/3, 8192/2);

    f32 angle = 0.f;
    //u16 delay = 1;
    f32 angle_increment = 10.f * 0.15915494309189535f / 1000.f;


    printf("Starting mag read...\r\n");

    //u16 tx_cmd = (1 << 14) | 0x3FFC; // DIAAGC reg
    //tx_cmd |= (__builtin_popcount(tx_cmd) << 15); // Even parity
    //u8 spi_tx_buf[] = {(u8) ((tx_cmd >> 8) & 0xFF), (u8) (tx_cmd & 0xFF)};
    //HAL_SPI_Transmit(encoder_spi_handle,spi_tx_buf,2,0xFFFF);
    //u8 spi_rx_buf[] = {0xAA, 0xAA};
    //HAL_SPI_Receive(encoder_spi_handle, spi_rx_buf, 2, 0xFFFF);

    u16 run_num = 0;

    as5047p_setup_dma_transaction();
    as5047p_preheat();

    //encoder_spi_read_benchmark();

    // Automated DMA test
    /*
    while (1) {
        printf("\u033c\033[2J\033[300A");

        as5047p_trigger_dma_read();
        while (encoder_dma_in_progress);

        if (encoder_read_success_dma)
            printf("Raw angle: %d\r\n", current_angle_dma_raw);
        else
            printf("Read fail \r\n");

        //printf("RX Parity: %d==%d\r\n", parity_bit, calc_parity);
        //printf("RX Success: %d\r\n", success_bit);
        //printf("Data Section: %d\r\n", data_section);

        HAL_Delay(10);

    }
     */


    // Manual DMA test
    /*
    while (1) {
        printf("\u033c\033[2J\033[300A");
        u16 spi_tx_buf[] = {0xFFFF};
        u16 spi_rx_buf[] = {0};
        gpio_pin_write(encoder_spi_nss, false);
        HAL_SPI_TransmitReceive_DMA(encoder_spi_handle, (u8 *) spi_tx_buf, (u8 *) spi_rx_buf, 1);
        //while (encoder_spi_handle->State != HAL_SPI_STATE_READY);
        while (encoder_spi_handle->State != HAL_SPI_STATE_READY);
        gpio_pin_write(encoder_spi_nss, true);

        u8 parity_bit = (spi_rx_buf[0] >> 15) & 0b1;
        u16 parity_section = spi_rx_buf[0] & 0x7FFF;
        u8 calc_parity = ams_parity(parity_section);
        u8 success_bit = (spi_rx_buf[0] >> 14) & 0b1;
        u16 data_section = spi_rx_buf[0] & 0x3FFF;
        printf("RX Parity: %d==%d\r\n", parity_bit, calc_parity);
        printf("RX Success: %d\r\n", success_bit);
        printf("Data Section: %d\r\n", data_section);

        HAL_Delay(10);

        //gpio_pin_write(encoder_spi_nss, true);
    }
     */

    // Manual tx/rx test
    /*
    while (1) {
        printf("\u033c\033[2J\033[300A");

        u16 spi_tx_buf[] = {0xFFFF};
        u16 spi_rx_buf[] = {0};

        gpio_pin_write(encoder_spi_nss, false);
        HAL_SPI_TransmitReceive(encoder_spi_handle, (u8 *) spi_tx_buf, (u8 *) spi_rx_buf, 1, 0xFFFF);
        gpio_pin_write(encoder_spi_nss, true);

        u8 parity_bit = (spi_rx_buf[0] >> 15) & 0b1;
        u16 parity_section = spi_rx_buf[0] & 0x7FFF;
        u8 calc_parity = ams_parity(parity_section);
        u8 success_bit = (spi_rx_buf[0] >> 14) & 0b1;
        u16 data_section = spi_rx_buf[0] & 0x3FFF;
        printf("RX Parity: %d==%d\r\n", parity_bit, calc_parity);
        printf("RX Success: %d\r\n", success_bit);
        printf("Data Section: %d\r\n", data_section);

        HAL_Delay(10);

    }
     */

    HAL_StatusTypeDef s1 = HAL_ADC_RegisterCallback(&hadc1, HAL_ADC_INJ_CONVERSION_COMPLETE_CB_ID, &ADC1_ISR);
    HAL_StatusTypeDef s2 = HAL_ADC_RegisterCallback(&hadc2, HAL_ADC_INJ_CONVERSION_COMPLETE_CB_ID, &ADC2_ISR);
    HAL_StatusTypeDef s3 = HAL_ADC_RegisterCallback(&hadc3, HAL_ADC_INJ_CONVERSION_COMPLETE_CB_ID, &ADC3_ISR);
    HAL_StatusTypeDef s4 = HAL_ADC_RegisterCallback(&hadc4, HAL_ADC_INJ_CONVERSION_COMPLETE_CB_ID, &ADC4_ISR);

    printf("Calibrating ADC... ");
    HAL_ADCEx_Calibration_Start(&hadc1, ADC_DIFFERENTIAL_ENDED);
    HAL_ADCEx_Calibration_Start(&hadc2, ADC_DIFFERENTIAL_ENDED);
    HAL_ADCEx_Calibration_Start(&hadc3, ADC_SINGLE_ENDED);
    HAL_ADCEx_Calibration_Start(&hadc4, ADC_DIFFERENTIAL_ENDED);
    printf("Done\r\n");

    HAL_ADC_RegisterCallback(&hadc1, HAL_ADC_CONVERSION_COMPLETE_CB_ID, &ADC1_ISR);
    HAL_ADC_RegisterCallback(&hadc2, HAL_ADC_CONVERSION_COMPLETE_CB_ID, &ADC2_ISR);
    HAL_ADC_RegisterCallback(&hadc3, HAL_ADC_CONVERSION_COMPLETE_CB_ID, &ADC3_ISR);
    HAL_ADC_RegisterCallback(&hadc4, HAL_ADC_CONVERSION_COMPLETE_CB_ID, &ADC4_ISR);

    hrtim_pwm_start();
    hrtim_pwm_set_dcycle_u16(4096, 0, 2048);

    HAL_StatusTypeDef stat_1 = HAL_ADCEx_InjectedStart(&hadc1);
    //printf("1: %d\r\n", stat_1);
    HAL_StatusTypeDef stat_2 = HAL_ADCEx_InjectedStart(&hadc2);
    //printf("2: %d\r\n", stat_2);
    HAL_StatusTypeDef stat_3 = HAL_ADCEx_InjectedStart(&hadc3);
    //printf("3: %d\r\n", stat_3);
    HAL_StatusTypeDef stat_4 = HAL_ADCEx_InjectedStart(&hadc4);
    //printf("4: %d\r\n", stat_4);

    while (1) {
        printf("\u033c\033[2J\033[300A");


        f32 v_vref = 3.0f;

        u16 adc_1_val = HAL_ADCEx_InjectedGetValue(&hadc1, ADC_INJECTED_RANK_1);
        u16 adc_2_val = HAL_ADCEx_InjectedGetValue(&hadc2, ADC_INJECTED_RANK_1);
        u16 adc_3_val = HAL_ADCEx_InjectedGetValue(&hadc3, ADC_INJECTED_RANK_1);
        u16 adc_4_val = HAL_ADCEx_InjectedGetValue(&hadc4, ADC_INJECTED_RANK_1);

        f32 adc_1_val_V = v_vref * ((f32) adc_1_val) / 4095.f;
        f32 adc_2_val_V = v_vref * ((f32) adc_2_val) / 4095.f;
        f32 adc_3_val_V = v_vref * ((f32) adc_3_val) / 4095.f;
        f32 adc_4_val_V = v_vref * ((f32) adc_4_val) / 4095.f;

        f32 vbus_divider_scale = 1.f / (10.2f / (10.2f + 35.7f));
        f32 vbus_v = adc_3_val_V * vbus_divider_scale;

        f32 ina240_midpt = v_vref / 2;
        f32 shunt_value = 0.01f;  // 10mohm
        f32 ina240_gain = 20.f; // 20x gain

        f32 pha_current = ((adc_2_val_V - ina240_midpt) / ina240_gain) / shunt_value;
        f32 phb_current = ((adc_1_val_V - ina240_midpt) / ina240_gain) / shunt_value;
        f32 phc_current = ((adc_4_val_V - ina240_midpt) / ina240_gain) / shunt_value;

        printf("VBus: %.2fV \r\n", vbus_v);
        printf("PHA Current: %.2fA \r\n", pha_current);
        printf("PHB Current: %.2fA \r\n", phb_current);
        printf("PHC Current: %.2fA \r\n", phc_current);

        HAL_ADC_Start_IT(&hadc1);
        HAL_ADC_Start_IT(&hadc2);
        HAL_ADC_Start_IT(&hadc3);
        HAL_ADC_Start_IT(&hadc4);

        while (!(READ_BIT((&hadc1)->State, HAL_ADC_STATE_REG_EOC)));
        while (!(READ_BIT((&hadc2)->State, HAL_ADC_STATE_REG_EOC)));
        while (!(READ_BIT((&hadc3)->State, HAL_ADC_STATE_REG_EOC)));
        while (!(READ_BIT((&hadc4)->State, HAL_ADC_STATE_REG_EOC)));
        //while ((&hadc1)->State != HAL_ADC_STATE_READY);
        //while ((&hadc2)->State != HAL_ADC_STATE_READY);
        //while ((&hadc3)->State != HAL_ADC_STATE_READY);
        //while ((&hadc4)->State != HAL_ADC_STATE_READY);

        //HAL_ADC_PollForConversion(&hadc1, 0xFFFF);
        //HAL_ADC_PollForConversion(&hadc2, 0xFFFF);
        //HAL_ADC_PollForConversion(&hadc3, 0xFFFF);
        //HAL_ADC_PollForConversion(&hadc4, 0xFFFF);

        adc_1_val = HAL_ADC_GetValue(&hadc1);
        adc_2_val = HAL_ADC_GetValue(&hadc2);
        adc_3_val = HAL_ADC_GetValue(&hadc3);
        adc_4_val = HAL_ADC_GetValue(&hadc4);

        adc_1_val_V = v_vref * ((f32) adc_1_val) / 4095.f;
        adc_2_val_V = v_vref * ((f32) adc_2_val) / 4095.f;
        adc_3_val_V = v_vref * ((f32) adc_3_val) / 4095.f;
        adc_4_val_V = v_vref * ((f32) adc_4_val) / 4095.f;

        vbus_divider_scale = 1.f / (10.2f / (10.2f + 35.7f));
        vbus_v = adc_3_val_V * vbus_divider_scale;

        ina240_midpt = v_vref / 2;
        shunt_value = 0.01f;  // 10mohm
        ina240_gain = 20.f; // 20x gain

        pha_current = ((adc_2_val_V - ina240_midpt) / ina240_gain) / shunt_value;
        phb_current = ((adc_1_val_V - ina240_midpt) / ina240_gain) / shunt_value;
        phc_current = ((adc_4_val_V - ina240_midpt) / ina240_gain) / shunt_value;

        printf("VBus: %.2fV \r\n", vbus_v);
        printf("PHA Current: %.2fA \r\n", pha_current);
        printf("PHB Current: %.2fA \r\n", phb_current);
        printf("PHC Current: %.2fA \r\n", phc_current);

        HAL_Delay(100);
    }



    /* USER CODE END 2 */

    /* Infinite loop */
    /* USER CODE BEGIN WHILE */
    while (1) {
        angle += angle_increment;
        angle = fmodf(angle, 2.f * ((f32) M_PI));

        f32 sin = sin_f32(angle) * 0.1f;
        f32 cos = cos_f32(angle) * 0.1f;

        svm_magnitudes_t svm = SVM(sin, cos);
        hrtim_pwm_set_dcycle_u16(4096, 0, 2048);

        HAL_Delay(100);

    }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void) {
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

    /** Configure the main internal regulator output voltage
    */
    HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1_BOOST);
    /** Initializes the RCC Oscillators according to the specified parameters
    * in the RCC_OscInitTypeDef structure.
    */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
    RCC_OscInitStruct.PLL.PLLN = 20;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
    RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
    RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
        Error_Handler();
    }
    /** Initializes the CPU, AHB and APB buses clocks
    */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
                                  | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK) {
        Error_Handler();
    }
}

/* USER CODE BEGIN 4 */
void user_assert_fail(uint8_t *file, uint32_t line) {
    printf("Assert failed: file %s on line %lu\r\n", file, line);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void) {
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
    __disable_irq();
    while (1) {
    }
    /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line) {
    /* USER CODE BEGIN 6 */
    printf("Wrong parameters value: file %s on line %lu\r\n", file, line);
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* USER CODE END 6 */
}

#endif /* USE_FULL_ASSERT */


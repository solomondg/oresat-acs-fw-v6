//
// Created by solomon on 2/16/22.
//

//
// Created by solomon on 1/8/22.
//

#include "bldc_mathutil.h"

#include "arm_math.h"
#include "arm_common_tables.h"
#include "cordic.h"

#include "periphmap.h"

#include "utils.h"

//#define M_PI_f32 ((f32) M_PI)


// Decls
sincos_t sincos_cordic(f32 x);

sincos_t sincos_cordic_raw(f32 x);

sincos_t sincos_cmsis(f32 x);

sincos_t sincos_cmath(f32 x);

f32 sin_cmsis(f32 x);

f32 cos_cmsis(f32 x);

f32 sin_cmath(f32 x);

f32 cos_cmath(f32 x);

// Our chosen function implementations of sin/cos
__sincos_attr f32 sin_f32(const f32 x) {
#if defined(SINCOS_FNCS_USE_CMSIS)
    return sin_cmsis(x);
#elif defined(SINCOS_FNCS_USE_CMATH)
    return sin_cmath(x);
#elif defined(SINCOS_FNCS_USE_CORDIC)
    return sin_cordic(x);
#endif
}

__sincos_attr f32 cos_f32(const f32 x) {
#if defined(SINCOS_FNCS_USE_CMSIS)
    return cos_cmsis(x);
#elif defined(SINCOS_FNCS_USE_CMATH)
    return cos_cmath(x);
#elif defined(SINCOS_FNCS_USE_CORDIC)
    return cos_cordic(x);
#endif
}

__sincos_attr sincos_t sincos_f32(const f32 x) {
#if defined(SINCOS_FNCS_USE_CMSIS)
    return sincos_cmsis(x);
#elif defined(SINCOS_FNCS_USE_CMATH)
    return sincos_cmath(x);
#elif defined(SINCOS_FNCS_USE_CORDIC)
    return sincos_cordic(x);
#endif
}

__cordic_fn_attr sincos_t sincos_cordic(const f32 x) { // TODO should we flatten this? squish da cordic.
    // x is angle, in radians
    f32 x_local = x;

    // Wrap to +/- 2pi (+/-360deg)
    while (x_local > 2.f * M_PI_f32) x_local -= 2.f * M_PI_f32;
    while (x_local < -2.f * M_PI_f32) x_local += 2.f * M_PI_f32;

    // Wrap to +/- pi (+/-180deg)
    if (x_local > M_PI_f32) x_local = x_local - 2.f * M_PI_f32;
    if (x_local < -M_PI_f32) x_local = x_local + 2.f * M_PI_f32;

    // Divide x by pi to get the +/- 1 range CORDIC uses

    f32 x_scale1 = x_local / M_PI_f32;

    q31_t x_q31;
    arm_float_to_q31(&x_scale1, &x_q31, 1u);

    // CORDIC config in user code cordic.c. Pray to god it got set properly.

    q31_t cordic_in[1] = {x_q31};
    q31_t cordic_out[2] = {0, 0};

    assert_param(HAL_CORDIC_GetState(cordic_handle) == HAL_CORDIC_STATE_READY);

    HAL_CORDIC_Calculate(cordic_handle, cordic_in, cordic_out, 1, 0xFFFF);

    f32 cordic_out_float[2] = {0.f, 0.f};
    arm_q31_to_float(cordic_out, cordic_out_float, 2);

    sincos_t ret;
    ret.cos = cordic_out_float[0];
    ret.sin = cordic_out_float[1];
    return ret;
}

// Don't even touch this
/*
sincos_t sincos_cordic_raw(const f32 x) {
    sincos_t ret;
    u32 cordicin = 0x7FFF0000;
    i16 thetashort = x * 10430;
    cordicin += thetashort;
    hcordic.Instance->WDATA = cordicin;
    u32 out = hcordic.Instance->RDATA;
    i16 out2 = (out & 0xFFFF0000) >> 16;
    i16 out1 = (out & 0xFFFF);
    ret.cos = (f32) out1 / 32768.f;
    ret.sin = (f32) out2 / 32768.f;
    return ret;
}
 */

__cmsis_fn_attr sincos_t sincos_cmsis(const f32 x) {
    sincos_t ret;
    arm_sin_cos_f32(x, &ret.sin, &ret.cos);
    return ret;
}

__cmath_fn_attr sincos_t sincos_cmath(const f32 x) {
    sincos_t ret;
    ret.sin = sin_cmath(x);
    ret.cos = cos_cmath(x);
    return ret;
}

__cmsis_fn_attr f32 sin_cmsis(const f32 x) {
    return arm_sin_f32(x);
}

__cmsis_fn_attr f32 cos_cmsis(const f32 x) {
    return arm_cos_f32(x);
}

__cmath_fn_attr f32 sin_cmath(const f32 x) {
    return sinf(x);
}

__cmath_fn_attr f32 cos_cmath(const f32 x) {
    return cosf(x);
}

__fn_const i32 round_f32(const f32 x) {
    return (i32) roundf(x);
}
__fn_const f32 abs_f32(const f32 x) {
    return fabsf(x);
}

__fn_const __fn_leaf f32 min_f32(f32 a, f32 b) {
    if (a < b) return a;
    return b;
}

__fn_const __fn_leaf f32 max_f32(f32 a, f32 b) {
    if (a > b) return a;
    return b;
}

__fn_const __fn_flatten f32 clamp_f32(f32 lower, f32 x, f32 upper) {
    return max_f32(min_f32(upper, x), lower);
}

__fn_const __fn_flatten f32 fast_atan2(f32 y, f32 x) {
    f32 abs_y = abs_f32(y);
    f32 abs_x = abs_f32(x);
    f32 a = min_f32(abs_x, abs_y) / (max_f32(abs_x, abs_y) + FLT_MIN);
    f32 s = a * a;
    f32 r = ((-0.0464964749f * s + 0.15931422f) * s - 0.327622764f) * s * a + a;
    if (abs_y > abs_x) r = 1.57079637f - r;
    if (x < 0.f) r = 3.14159274f - r;
    if (y < 0.f) r = -r;
    return r;
}


// SVM transform: A/B stator-oriented quadrature to PWM timings
__fn_const __fn_leaf svm_magnitudes_t SVM(const f32 alpha, const f32 beta) {
    f32 tA, tB, tC;
    i32 sextant;
//#define one_over_sqrt3 0.57735026919f
//#define two_over_sqrt3 1.15470053838f

    if (beta >= 0.f) {
        if (alpha >= 0.f)
            sextant = (one_over_sqrt3 * beta > alpha) ? 2 : 1;
        else
            sextant = (-one_over_sqrt3 * beta > alpha) ? 3 : 2;
    } else {
        if (alpha >= 0.f)
            sextant = (-one_over_sqrt3 * beta > alpha) ? 5 : 6;
        else {
            sextant = (one_over_sqrt3 * beta > alpha) ? 4 : 5;
        }
    }

    switch (sextant) {
        case 1: {
            f32 t1 = alpha - one_over_sqrt3 * beta;
            f32 t2 = two_over_sqrt3 * beta;
            tA = (1.f - t1 - t2) * .5f;
            tB = tA + t1;
            tC = tB + t2;
        }
            break;
        case 2 : {
            f32 t2 = alpha + one_over_sqrt3 * beta;
            f32 t3 = -alpha + one_over_sqrt3 * beta;

            tB = (1.f - t2 - t3) * .5f;
            tA = tB + t3;
            tC = tA + t2;
        }
            break;
        case 3: {
            f32 t3 = two_over_sqrt3 * beta;
            f32 t4 = -alpha - one_over_sqrt3 * beta;

            tB = (1.f - t3 - t4) * .5f;
            tC = tB + t3;
            tA = tC + t4;
        }
            break;
        case 4: {
            f32 t4 = -alpha + one_over_sqrt3 * beta;
            f32 t5 = -two_over_sqrt3 * beta;

            tC = (1.0f - t4 - t5) * .5f;
            tB = tC + t5;
            tA = tB + t4;
        }
            break;
        case 5: {
            f32 t5 = -alpha - one_over_sqrt3 * beta;
            f32 t6 = alpha - one_over_sqrt3 * beta;

            tC = (1.f - t5 - t6) * .5f;
            tA = tC + t5;
            tB = tA + t6;
        }
            break;
        case 6: {
            f32 t6 = -two_over_sqrt3 * beta;
            f32 t1 = alpha + one_over_sqrt3 * beta;

            tA = (1.f - t6 - t1) * .5f;
            tC = tA + t1;
            tB = tC + t6;
        }
            break;
    }

    svm_magnitudes_t ret = {tA, tB, tC};
    return ret;

}

__fn_const __fn_leaf bool svm_output_valid(const svm_magnitudes_t svm_magnitudes) {
    return svm_magnitudes.A >= 0.f && svm_magnitudes.A <= 1.f
           && svm_magnitudes.B >= 0.f && svm_magnitudes.B <= 1.f
           && svm_magnitudes.C >= 0.f && svm_magnitudes.C <= 1.f;
}


//
// Created by solomon on 2/17/22.
//

#include "utils.h"
#include "stm32g4xx_hal.h"
#include "periphmap.h"


u32 micros() {
    u32 ms;
    u32 cycle_cnt;
    do {
        ms = HAL_GetTick();
        cycle_cnt = timebase_source_timer->CNT;
    } while (ms != HAL_GetTick());

    return (ms * 1000) + cycle_cnt;
}

void delay_us(const u32 us) {
    u32 start = micros();
    while (micros() - start < us) {
        asm volatile ("nop");
    }
}

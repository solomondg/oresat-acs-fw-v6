//
// Created by solomon on 2/21/22.
//

#include "as5047p.h"
#include "periphmap.h"

#include "stm32g4xx_it.h"

#include "printf.h"

#define AS5047P_ERROR_REG_ADDR 0x1
#define AS5047P_DIAG_REG_ADDR 0x3FFC
#define AS5047P_MAG_REG_ADDR 0x3FFD
#define AS5047P_ANGLEUNC_REG_ADDR 0x3FFE
#define AS5047P_ANGLECOM_REG_ADDR 0x3FFF

#define AS5047P_WRITE_CMD (0 << 14)
#define AS5047P_READ_CMD (1 << 14)

static volatile bool angle_measurement_called_last = false;

__fn_hot __fn_inline __fn_flatten __fn_const u8 ams_parity(u16 v) {
    v ^= v >> 8;
    v ^= v >> 4;
    v ^= v >> 2;
    v ^= v >> 1;
    return v & 1;
}

static __fn_inline __fn_flatten as5047p_read_status_t as5047p_read_register(u16 reg_addr, u16 *data) {
    angle_measurement_called_last = false;

    u16 tx_cmd = AS5047P_READ_CMD | reg_addr;

    //u16 tx_parity_count = 0;
    //for (u8 i = 0; i < 15; i++)
    //    tx_parity_count += (tx_cmd >> i) & 0b1;
    u16 tx_parity_bit = ams_parity(tx_cmd);

    u16 tx_cmd_with_parity = tx_cmd | (tx_parity_bit << 15);

    u16 tx_buf[1] = {tx_cmd_with_parity};
    u16 rx_buf[1] = {0};
    // Run spi transaction twice, to ensure we're reading the correct register
    for (u8 i = 0; i < 2; i++) {

        rx_buf[0] = 0;

        gpio_pin_write(encoder_spi_nss, false);
        HAL_StatusTypeDef write_status = HAL_SPI_TransmitReceive(encoder_spi_handle, (u8 *) tx_buf, (u8 *) rx_buf, 1,
                                                                 0xFFFF);
        gpio_pin_write(encoder_spi_nss, true);

        if (write_status != HAL_OK) {
            as5047p_read_status_t initial_write_err;
            switch (write_status) {
                case HAL_ERROR:
                    initial_write_err = AS5047P_READ_HAL_ERROR;
                    break;
                case HAL_BUSY:
                    initial_write_err = AS5047P_READ_HAL_BUSY;
                    break;
                case HAL_TIMEOUT:
                    initial_write_err = AS5047P_READ_HAL_TIMEOUT;
                    break;
                case HAL_OK:
                default:
                    initial_write_err = AS5047P_SOLOMON_FIX_UR_CODE;
                    assert(false);
                    break;
            }
            return initial_write_err;
        }
    }

    u16 success_bit = (rx_buf[0] >> 14) & 0b1;
    u16 parity_bit = (rx_buf[0] >> 15) & 0b1;
    u16 frame_data = rx_buf[0] & 0x3FFF;
    u16 parity_data = rx_buf[0] & 0x7FFF;
    u8 computed_parity = ams_parity(parity_data);

    if (computed_parity != parity_bit)
        return AS5047P_READ_PARITY_ERROR;
    if (success_bit == 1)
        return AS5047P_READ_ERROR;

    *data = frame_data;

    return AS5047P_READ_OK;
}


as5047p_read_status_t __fn_flatten as5047p_read_error_reg(AS5047P_REG_ERROR_t *reg_error) {
    u16 retn = 0;
    as5047p_read_status_t stat = as5047p_read_register(AS5047P_ERROR_REG_ADDR, &retn);
    if (stat == AS5047P_READ_OK) {
        reg_error->FRAMING_ERROR = (retn >> 2) & 0b1;
        reg_error->INVALID_COMMAND_ERROR = (retn >> 1) & 0b1;
        reg_error->PARITY_ERROR = retn & 0b1;
    }
    return stat;
}

as5047p_read_status_t __fn_flatten as5047p_read_diag_reg(AS5047P_REG_DIAG_t *reg_diag) {
    u16 retn = 0;
    as5047p_read_status_t stat = as5047p_read_register(AS5047P_DIAG_REG_ADDR, &retn);
    if (stat == AS5047P_READ_OK) {
        reg_diag->FIELD_TOO_LOW = (retn >> 11) & 0b1;
        reg_diag->FIELD_TOO_HIGH = (retn >> 10) & 0b1;
        reg_diag->CORDIC_OVERFLOW = (retn >> 9) & 0b1;
        reg_diag->OFFSET_COMPENSATION_READY = (retn >> 8) & 0b1;
        reg_diag->AGC_VALUE = retn & 0xFF;
    }
    return stat;
}

as5047p_read_status_t __fn_flatten as5047p_read_cordic_magnitude_reg(AS5047P_REG_MAG_t *reg_magnitude) {
    u16 retn = 0;
    as5047p_read_status_t stat = as5047p_read_register(AS5047P_MAG_REG_ADDR, &retn);
    if (stat == AS5047P_READ_OK) {
        reg_magnitude->CORDIC_MAGNITUDE = retn & 0x3FFF;
    }
    return stat;

}

as5047p_read_status_t __fn_flatten
as5047p_read_angle_uncompensated_reg(AS5047P_REG_ANGLEUNC_t *reg_angle_uncompensated) {
    u16 retn = 0;
    as5047p_read_status_t stat = as5047p_read_register(AS5047P_ANGLEUNC_REG_ADDR, &retn);
    if (stat == AS5047P_READ_OK) {
        reg_angle_uncompensated->ANGLE_UNCOMPENSATED = retn & 0x3FFF;
    }
    return stat;
}

as5047p_read_status_t __fn_flatten as5047p_read_angle_compensated_reg(AS5047P_REG_ANGLECOM_t *reg_angle_compensated) {
    u16 retn = 0;
    as5047p_read_status_t stat = as5047p_read_register(AS5047P_ANGLECOM_REG_ADDR, &retn);
    if (stat == AS5047P_READ_OK) {
        reg_angle_compensated->ANGLE_COMPENSATED = retn & 0x3FFF;
    }
    return stat;
}

static volatile u16 dma_angle_rx[1] = {0};
static volatile u16 dma_angle_tx[1] = {0xFFFF};

volatile u16 current_angle_dma_raw = 0;
volatile u32 angle_timestamp_dma = 0;
volatile u32 angle_timestamp_completed_dma = 0;
volatile bool encoder_read_success_dma = false;
volatile bool encoder_dma_in_progress = false;

/*
// Do we need this?
void as5047p_dma_tx_complete_callback(DMA_HandleTypeDef *dma_handle) {
    encoder_dma_in_progress = false;
    gpio_pin_write(encoder_spi_nss, true);
    printf("DMA TX Complete\r\n");
}

// Should be called whenever DMA transaction is complete
void as5047p_dma_rx_complete_callback(DMA_HandleTypeDef *dma_handle) {
    encoder_dma_in_progress = false;
    gpio_pin_write(encoder_spi_nss, true);
    printf("DMA RX Complete\r\n");
}
 */

__fn_hot void as5047p_spi_txrx_complete_callback(SPI_HandleTypeDef *spi_handle) {
    //gpio_pin_write(encoder_spi_nss, true);
    encoder_spi_nss.port->BSRR = encoder_spi_nss.pin;

    const u16 data_rx = dma_angle_rx[0];

    const u16 parity_bit = (data_rx >> 15) & 0b1;
    const u16 success_bit = (data_rx >> 14) & 0b1;
    const u16 parity_section = data_rx & 0x7FFF;
    const u16 computed_parity = ams_parity(parity_section);

    if ((success_bit == 1) || (parity_bit != computed_parity)) {
        //printf("Read fail, raw data: %d\r\n", data_rx);
        //printf("Raw data &0x3FFF: %d\r\n", data_rx & 0x3FFF);


        //AS5047P_REG_ERROR_t reg_error;
        //as5047p_read_status_t read_stat = as5047p_read_error_reg(&reg_error);
        //printf("Error reg read status: %d\r\n", read_stat);
        //printf("PARARR: %d\r\n", reg_error.PARITY_ERROR);
        //printf("INVCOMM: %d\r\n", reg_error.INVALID_COMMAND_ERROR);
        //printf("FRERROR: %d\r\n", reg_error.FRAMING_ERROR);


        encoder_read_success_dma = false;
    } else {
        encoder_read_success_dma = true;

        u16 angle_data = data_rx & 0x3FFF;
        current_angle_dma_raw = angle_data;
        //current_angle_dma_f32 = 360.f * ((f32) angle_data) / 16383.f;
    }
    encoder_dma_in_progress = false;
}

void SPI_ERROR_CB() {
    printf("SPI ERROR\r\n");
    while (1);
}

void as5047p_setup_dma_transaction() {
    current_angle_dma_raw = 0;
    angle_timestamp_dma = 0;
    angle_timestamp_completed_dma = 0;
    encoder_read_success_dma = false;
    encoder_dma_in_progress = false;

    HAL_SPI_RegisterCallback(encoder_spi_handle, HAL_SPI_TX_RX_COMPLETE_CB_ID, &as5047p_spi_txrx_complete_callback);
    //HAL_DMA_RegisterCallback(encoder_spi_rx_dma, HAL_DMA_XFER_CPLT_CB_ID, &as5047p_dma_rx_complete_callback);
    //HAL_DMA_RegisterCallback(encoder_spi_tx_dma, HAL_DMA_XFER_CPLT_CB_ID, &as5047p_dma_tx_complete_callback);
    //HAL_SPI_TransmitReceive_DMA()
}

__fn_inline __fn_flatten static HAL_StatusTypeDef
HAL_SPI_TransmitReceive_DMA_Smol(SPI_HandleTypeDef *hspi, uint8_t *pTxData, uint8_t *pRxData, uint16_t Size);

__fn_flatten __fn_hot bool as5047p_trigger_dma_read() {
    encoder_dma_in_progress = true;
    if (!angle_measurement_called_last) {
        encoder_dma_in_progress = false;
        return false;
    }
    dma_angle_rx[0] = 0;
    //gpio_pin_write(encoder_spi_nss, false);
    encoder_spi_nss.port->BRR = encoder_spi_nss.pin;
    HAL_StatusTypeDef stat = HAL_SPI_TransmitReceive_DMA(encoder_spi_handle, (u8 *) dma_angle_tx,
                                                              (u8 *) dma_angle_rx,
                                                              1);
    return (stat == HAL_OK);
}

// Calls SPI transaction once
void as5047p_preheat() {
    angle_measurement_called_last = true;

    u16 tx_buf[] = {0xFFFF};
    u16 rx_buf[] = {0};

    gpio_pin_write(encoder_spi_nss, false);
    HAL_StatusTypeDef write_status = HAL_SPI_TransmitReceive(encoder_spi_handle, (u8 *) tx_buf, (u8 *) rx_buf, 1,
                                                             0xFFFF);
    gpio_pin_write(encoder_spi_nss, true);
}

void encoder_spi_read_benchmark() {
    u32 success_cnt = 0;
    u32 start_ts = HAL_GetTick();
    u32 n = 100000;
    for (u32 i = 0; i < n; i++) {
        as5047p_trigger_dma_read();
        while (encoder_dma_in_progress);
        if (encoder_read_success_dma) success_cnt++;
    }
    u32 end_ts = HAL_GetTick();
    u32 dt = end_ts - start_ts;
    printf("Total time: %d ms\r\n", dt);
    printf("Total xactions: %d\r\n", n);
    printf("Average: %.4f kHz\r\n", ((f32) n) / ((f32) dt));
    printf("Success: %d/%d (%f)\r\n", success_cnt, n, ((f32) success_cnt) / ((f32) n));
}

#include "stm32g4xx_hal_spi.h"
#include "stm32g4xx_hal.h"

static void SPI_DMATransmitReceiveCplt(DMA_HandleTypeDef *hdma) {

    as5047p_spi_txrx_complete_callback(encoder_spi_handle);
}


__fn_inline __fn_flatten __fn_hot static void
DMA_SetConfig_Smol(DMA_HandleTypeDef *hdma, uint32_t SrcAddress, uint32_t DstAddress, uint32_t DataLength) {
    ///* Clear the DMAMUX synchro overrun flag */
    //hdma->DMAmuxChannelStatus->CFR = hdma->DMAmuxChannelStatusMask;

    //if (hdma->DMAmuxRequestGen != 0U) {
    //    /* Clear the DMAMUX request generator overrun flag */
    //    hdma->DMAmuxRequestGenStatus->RGCFR = hdma->DMAmuxRequestGenStatusMask;
    //}

    /* Clear all flags */
    hdma->DmaBaseAddress->IFCR = (DMA_ISR_GIF1 << (hdma->ChannelIndex & 0x1FU));

    /* Configure DMA Channel data length */
    hdma->Instance->CNDTR = DataLength;

    /* Memory to Peripheral */
    if ((hdma->Init.Direction) == DMA_MEMORY_TO_PERIPH) {
        /* Configure DMA Channel destination address */
        hdma->Instance->CPAR = DstAddress;

        /* Configure DMA Channel source address */
        hdma->Instance->CMAR = SrcAddress;
    }
        /* Peripheral to Memory */
    else {
        /* Configure DMA Channel source address */
        hdma->Instance->CPAR = SrcAddress;

        /* Configure DMA Channel destination address */
        hdma->Instance->CMAR = DstAddress;
    }
}

__fn_inline __fn_flatten __fn_hot static HAL_StatusTypeDef
HAL_DMA_Start_IT_Smol(DMA_HandleTypeDef *hdma, uint32_t SrcAddress, uint32_t DstAddress,
                      uint32_t DataLength) {
    HAL_StatusTypeDef status = HAL_OK;

    /* Check the parameters */


    /* Change DMA peripheral state */
    hdma->State = HAL_DMA_STATE_BUSY;
    hdma->ErrorCode = HAL_DMA_ERROR_NONE;

    /* Disable the peripheral */
    __HAL_DMA_DISABLE(hdma);

    /* Configure the source, destination address and the data length & clear flags*/
    DMA_SetConfig_Smol(hdma, SrcAddress, DstAddress, DataLength);

    __HAL_DMA_DISABLE_IT(hdma, DMA_IT_HT);
    __HAL_DMA_ENABLE_IT(hdma, (DMA_IT_TC | DMA_IT_TE));

    ///* Check if DMAMUX Synchronization is enabled*/
    //if ((hdma->DMAmuxChannel->CCR & DMAMUX_CxCR_SE) != 0U) {
    //    /* Enable DMAMUX sync overrun IT*/
    //    hdma->DMAmuxChannel->CCR |= DMAMUX_CxCR_SOIE;
    //}

    //if (hdma->DMAmuxRequestGen != 0U) {
    //    /* if using DMAMUX request generator, enable the DMAMUX request generator overrun IT*/
    //    /* enable the request gen overrun IT*/
    //    hdma->DMAmuxRequestGen->RGCR |= DMAMUX_RGxCR_OIE;
    //}

    /* Enable the Peripheral */
    __HAL_DMA_ENABLE(hdma);
    return status;
}

__fn_inline __fn_flatten __fn_hot static HAL_StatusTypeDef
HAL_SPI_TransmitReceive_DMA_Smol(SPI_HandleTypeDef * restrict hspi, uint8_t * restrict pTxData, uint8_t * restrict pRxData,
                                 const uint16_t Size) {
    uint32_t tmp_mode;
    HAL_SPI_StateTypeDef tmp_state;
    HAL_StatusTypeDef errorcode = HAL_OK;

    /* Init temporary variables */
    tmp_state = hspi->State;
    tmp_mode = hspi->Init.Mode;

    /* Set the transaction information */
    hspi->ErrorCode = HAL_SPI_ERROR_NONE;
    hspi->pTxBuffPtr = (uint8_t *) pTxData;
    hspi->TxXferSize = Size;
    hspi->TxXferCount = Size;
    hspi->pRxBuffPtr = (uint8_t *) pRxData;
    hspi->RxXferSize = Size;
    hspi->RxXferCount = Size;

    /* Init field not used in handle to zero */
    hspi->RxISR = NULL;
    hspi->TxISR = NULL;


    /* Reset the threshold bit */
    CLEAR_BIT(hspi->Instance->CR2, SPI_CR2_LDMATX | SPI_CR2_LDMARX);

    /* The packing mode management is enabled by the DMA settings according the spi data size */
    /* Set fiforxthreshold according the reception data length: 16bit */
    CLEAR_BIT(hspi->Instance->CR2, SPI_RXFIFO_THRESHOLD);

    /* Set the SPI Tx/Rx DMA Half transfer complete callback */
    hspi->hdmarx->XferHalfCpltCallback = NULL;
    hspi->hdmarx->XferCpltCallback = &SPI_DMATransmitReceiveCplt;
    hspi->hdmarx->XferErrorCallback = NULL;
    hspi->hdmarx->XferAbortCallback = NULL;

    /* Enable the Rx DMA Stream/Channel  */
    HAL_DMA_Start_IT_Smol(hspi->hdmarx, (uint32_t) &hspi->Instance->DR, (uint32_t) hspi->pRxBuffPtr,
                          hspi->RxXferCount);

    /* Enable Rx DMA Request */
    SET_BIT(hspi->Instance->CR2, SPI_CR2_RXDMAEN);

    hspi->hdmatx->XferHalfCpltCallback = NULL;
    hspi->hdmatx->XferCpltCallback = NULL;
    hspi->hdmatx->XferErrorCallback = NULL;
    hspi->hdmatx->XferAbortCallback = NULL;

    /* Enable the Tx DMA Stream/Channel  */
    HAL_DMA_Start_IT_Smol(hspi->hdmatx, (uint32_t) hspi->pTxBuffPtr, (uint32_t) &hspi->Instance->DR,
                          hspi->TxXferCount);

    /* Enable the SPI Error Interrupt Bit */
    __HAL_SPI_ENABLE_IT(hspi, (SPI_IT_ERR));

    SET_BIT(hspi->Instance->CR2, SPI_CR2_TXDMAEN);

    error :
    return errorcode;
}

//
// Created by solomon on 2/28/22.
//

#include <math.h>
#include "pwm.h"
#include "utils.h"
#include "printf.h"

void hrtim_pwm_setup() {

}

__fn_flatten __fn_inline void hrtim_pwm_start() {
    //HAL_HRTIM_WaveformOutputStart(bldc_hrtim_handle,
    //                              phase_a_H_output | phase_a_L_output |
    //                              phase_b_H_output | phase_b_L_output |
    //                              phase_c_H_output | phase_c_L_output
    //);
    HAL_HRTIM_WaveformOutputStart(&hhrtim1,
                                  HRTIM_OUTPUT_TA1 | HRTIM_OUTPUT_TA2 |
                                  HRTIM_OUTPUT_TE1 | HRTIM_OUTPUT_TE2 |
                                  HRTIM_OUTPUT_TF1 | HRTIM_OUTPUT_TF2
    );

    HAL_HRTIM_WaveformCountStart(&hhrtim1,
                                 HRTIM_TIMERID_TIMER_A | HRTIM_TIMERID_TIMER_F | HRTIM_TIMERID_TIMER_E);

    //HAL_HRTIM_RegisterCallback(&hhrtim1,)
    //HAL_HRTIM_TIMxRegisterCallback()
}

__fn_flatten __fn_inline void hrtim_pwm_stop() {
    HAL_HRTIM_WaveformOutputStop(&hhrtim1,
                                 HRTIM_OUTPUT_TA1 | HRTIM_OUTPUT_TA2 |
                                 HRTIM_OUTPUT_TE1 | HRTIM_OUTPUT_TE2 |
                                 HRTIM_OUTPUT_TF1 | HRTIM_OUTPUT_TF2
    );

    HAL_HRTIM_WaveformCountStop(&hhrtim1,
                                HRTIM_TIMERID_TIMER_A | HRTIM_TIMERID_TIMER_F | HRTIM_TIMERID_TIMER_E);

    __HAL_HRTIM_SETCOUNTER(&hhrtim1, HRTIM_TIMERINDEX_TIMER_A, 0);
    __HAL_HRTIM_SETCOUNTER(&hhrtim1, HRTIM_TIMERINDEX_TIMER_E, 0);
    __HAL_HRTIM_SETCOUNTER(&hhrtim1, HRTIM_TIMERINDEX_TIMER_F, 0);

}

__fn_flatten __fn_inline void hrtim_pwm_set_dcycle_f32(const f32 a_dcycle, const f32 b_dcycle, const f32 c_dcycle) {
    //assert(a_dcycle >= 0.f && a_dcycle <= 1.f);
    //assert(b_dcycle >= 0.f && b_dcycle <= 1.f);
    //assert(c_dcycle >= 0.f && c_dcycle <= 1.f);
    f32 a_dcycle_local = CLAMP(a_dcycle, 0.f, 1.f);
    f32 b_dcycle_local = CLAMP(b_dcycle, 0.f, 1.f);
    f32 c_dcycle_local = CLAMP(c_dcycle, 0.f, 1.f);
    hrtim_pwm_set_dcycle_u16(
            (u16) (a_dcycle_local * ((f32) PWM_CNTR)),
            (u16) (b_dcycle_local * ((f32) PWM_CNTR)),
            (u16) (c_dcycle_local * ((f32) PWM_CNTR))
    );
}

__fn_flatten __fn_inline void hrtim_pwm_set_dcycle_u16(const u16 a_dcycle, const u16 b_dcycle, const u16 c_dcycle) {
    //assert(a_dcycle >= PWM_CNTR_MIN_ALLOWED && a_dcycle <= PWM_CNTR_MAX_ALLOWED);
    //assert(b_dcycle >= PWM_CNTR_MIN_ALLOWED && b_dcycle <= PWM_CNTR_MAX_ALLOWED);
    //assert(c_dcycle >= PWM_CNTR_MIN_ALLOWED && c_dcycle <= PWM_CNTR_MAX_ALLOWED);
    u16 a_compare = CLAMP(a_dcycle, PWM_CNTR_MIN_ALLOWED, PWM_CNTR_MAX_ALLOWED);
    u16 b_compare = CLAMP(b_dcycle, PWM_CNTR_MIN_ALLOWED, PWM_CNTR_MAX_ALLOWED);
    u16 c_compare = CLAMP(c_dcycle, PWM_CNTR_MIN_ALLOWED, PWM_CNTR_MAX_ALLOWED);
    __HAL_HRTIM_SETCOMPARE(&hhrtim1, HRTIM_TIMERINDEX_TIMER_A, HRTIM_COMPAREUNIT_1, a_compare);
    __HAL_HRTIM_SETCOMPARE(&hhrtim1, HRTIM_TIMERINDEX_TIMER_E, HRTIM_COMPAREUNIT_1, b_compare);
    __HAL_HRTIM_SETCOMPARE(&hhrtim1, HRTIM_TIMERINDEX_TIMER_F, HRTIM_COMPAREUNIT_1, c_compare);
}

//
// Created by solomon on 2/16/22.
//

#include "usart.h"
#include "periphmap.h"

int __io_putchar(int ch) {
    HAL_UART_Transmit(&huart3, (u8 *) &ch, 1, 0xFFFF);
    return ch;
}
void _putchar(char character) {
    HAL_UART_Transmit(&huart3, (u8 *) &character, 1, 0xFFFF);
}

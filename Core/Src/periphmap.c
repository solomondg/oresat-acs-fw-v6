//
// Created by solomon on 2/16/22.
//

#include "periphmap.h"


// ADC
ADC_HandleTypeDef *const phase_a_adc_handle = &hadc2;
ADC_HandleTypeDef *const phase_b_adc_handle = &hadc1;
ADC_HandleTypeDef *const phase_c_adc_handle = &hadc4;
ADC_HandleTypeDef *const vbus_singleend_adc_handle = &hadc3;

// CORDIC
CORDIC_HandleTypeDef *const cordic_handle = &hcordic;

// DAC
DAC_HandleTypeDef *const dac_aout1_handle = &hdac1;
const u32 dac_aout1_channel = DAC_CHANNEL_1;
DAC_HandleTypeDef *const dac_aout2_handle = &hdac2;
const u32 dac_aout2_channel = DAC_CHANNEL_2;

// DMA
extern DMA_HandleTypeDef hdma_adc1;
extern DMA_HandleTypeDef hdma_adc2;
extern DMA_HandleTypeDef hdma_adc3;
extern DMA_HandleTypeDef hdma_adc4;
//extern HRTIM_HandleTypeDef hhrtim1;
extern DMA_HandleTypeDef hdma_spi1_tx;
extern DMA_HandleTypeDef hdma_spi1_rx;
extern DMA_HandleTypeDef hdma_spi3_tx;
//extern TIM_HandleTypeDef htim1;
//extern TIM_HandleTypeDef htim15;


DMA_HandleTypeDef *const phase_a_adc_dma;
DMA_HandleTypeDef *const phase_b_adc_dma;
DMA_HandleTypeDef *const phase_c_adc_dma;
DMA_HandleTypeDef *const vbus_singleend_adc_dma;

DMA_HandleTypeDef *const leds_spi_tx_dma = &hdma_spi3_tx;

DMA_HandleTypeDef *const encoder_spi_tx_dma = &hdma_spi1_tx;
DMA_HandleTypeDef *const encoder_spi_rx_dma = &hdma_spi1_rx;


// FDCAN
FDCAN_HandleTypeDef *const can_handle = &hfdcan1;

// FMAC
FMAC_HandleTypeDef *const fmac_handle = &hfmac;

// GPIO
const gpio_pin_t gpio1_pin = (gpio_pin_t) {GPIO1_GPIO_Port, GPIO1_Pin};
const gpio_pin_t gpio2_pin = (gpio_pin_t) {GPIO2_GPIO_Port, GPIO2_Pin};
const gpio_pin_t gpio3_pin = (gpio_pin_t) {GPIO3_GPIO_Port, GPIO3_Pin};
const gpio_pin_t gpio4_pin = (gpio_pin_t) {GPIO4_GPIO_Port, GPIO4_Pin};

const gpio_pin_t encoder_spi_nss = (gpio_pin_t) {SPI_SOFTNSS_GPIO_Port, SPI_SOFTNSS_Pin};

// I2C
I2C_HandleTypeDef *const i2c_handle = &hi2c1;

// SPI
SPI_HandleTypeDef *const leds_spi_handle = &hspi3;
SPI_HandleTypeDef *const encoder_spi_handle = &hspi1;

// TIM
TIM_HandleTypeDef *const status_led_tim_handle = &htim4;
const u32 status_led_tim_channel = TIM_CHANNEL_1;

// USART
UART_HandleTypeDef *const debug_uart_handle = &huart3;

// HRTIM

HRTIM_HandleTypeDef *const bldc_hrtim_handle = &hhrtim1;
const u8 phase_a_hrtim_timer_handle = HRTIM_TIMERINDEX_TIMER_A; // Timer A
const u8 phase_b_hrtim_timer_handle = HRTIM_TIMERINDEX_TIMER_E; // Timer E
const u8 phase_c_hrtim_timer_handle = HRTIM_TIMERINDEX_TIMER_F; // Timer F
const u8 master_hrtim_timer_handle = HRTIM_TIMERINDEX_MASTER; // Master timer

const u32  phase_a_H_compare_unit = HRTIM_COMPAREUNIT_1; // TA1
const u32  phase_a_L_compare_unit = HRTIM_COMPAREUNIT_2; // TA2
const u32  phase_b_H_compare_unit = HRTIM_COMPAREUNIT_1; // TE1
const u32  phase_b_L_compare_unit = HRTIM_COMPAREUNIT_2; // TE2
const u32  phase_c_H_compare_unit = HRTIM_COMPAREUNIT_1; // TF1
const u32  phase_c_L_compare_unit = HRTIM_COMPAREUNIT_2; // TF2

const u32  phase_a_H_output = HRTIM_OUTPUT_TA1; // TA1
const u32  phase_a_L_output = HRTIM_OUTPUT_TA2; // TA2
const u32  phase_b_H_output = HRTIM_OUTPUT_TE1; // TE1
const u32  phase_b_L_output = HRTIM_OUTPUT_TE2; // TE2
const u32  phase_c_H_output = HRTIM_OUTPUT_TF1; // TF1
const u32  phase_c_L_output = HRTIM_OUTPUT_TF2; // TF2

// SYSTICK
TIM_TypeDef *const timebase_source_timer = TIM15;